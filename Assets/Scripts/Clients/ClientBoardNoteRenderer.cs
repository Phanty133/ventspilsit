﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientBoardNoteRenderer : MonoBehaviour
{
    public GameObject noteTemplate;

    void createNote(int clientID) {
        Vector2 notePosition = new Vector2();
        notePosition.x = Random.Range(5, 185);
        notePosition.y = Random.Range(-15, -85);

        ClientManager.clientNoteObj[clientID] = Instantiate(noteTemplate, transform); //Instantiate note object
        ClientManager.clientNoteObj[clientID].GetComponent<RectTransform>().anchoredPosition = notePosition; //Set the randomized note position
    }

    void removeNote(int clientID) {
        Destroy(ClientManager.clientNoteObj[clientID]); //Destroy the cured client note object
    }

    #region Event subscribtions
    private void OnEnable()
    {
        ClientManager.OnClientNew += createNote;
        ClientManager.OnClientCured += removeNote;
        ClientManager.OnClientDied += removeNote;
    }

    private void OnDisable()
    {
        ClientManager.OnClientNew -= createNote;
        ClientManager.OnClientCured -= removeNote;
        ClientManager.OnClientDied += removeNote;
    }
    #endregion
}
