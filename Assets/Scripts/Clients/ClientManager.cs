﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using static InventoryManager;
using System.Linq;

public class ClientManager : MonoBehaviour
{
    public static ClientList clients;

    public bool strictMedicineCheck = false; //Accept medicine only with the required symptoms - no more, no less

    #region Event definitions

    public delegate void ClientNew(int clientID);
    public static event ClientNew OnClientNew; //Invoked, when new client is generated

    public delegate void ClientCured(int clientID);
    public static event ClientCured OnClientCured; //Invoked, when a client has been cured

    public delegate void ClientDied(int clientID);
    public static event ClientDied OnClientDied; //Invoked, when a client has been cured

    public delegate void ClientsEmpty();
    public static event ClientsEmpty OnClientsEmpty; //Invoked, when there are no clients left

    #endregion

    public static List<Dictionary<string, int>> currentPatientSymptoms = new List<Dictionary<string, int>>(); //{ {SymptomID, SymptomOccurences} }
    public static Dictionary<int, GameObject> clientGameObjects = new Dictionary<int, GameObject>();
    public static Dictionary<int, GameObject> clientNoteObj = new Dictionary<int, GameObject>();
    public static Dictionary<int, Color> clientColor = new Dictionary<int, Color>();

    [Serializable]
    public class Client {
        public PropertyList symptoms { get; private set; }
        public PropertyList harmfulProperties { get; private set; }
        public string name;
        //public GameObject gameObject;
        public int id;
        //public GameObject noteObj; //The client board note game object
        //public Color color;

        //private InventoryManager invManager;
        //private GameObject clManager;
        private Item medicineItem; //Item, that has been given to the patient

        public Client(PropertyList initSymptoms, PropertyList initHarmfulProperties = null, string clientName = "", GameObject clientObject = null, int id = -1, Color imageColor = new Color()) //Custom client
        {
            symptoms = initSymptoms;
            harmfulProperties = initHarmfulProperties == null ? new PropertyList() : initHarmfulProperties; //Set it as an empty PropertyList, if no properties given
            name = clientName;
            clientGameObjects[id] = clientObject;

            //invManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
            //clManager = GameObject.Find("ClientManager");

            clientColor[id] = imageColor.Equals(new Color()) ? new Color(1, 1, 1) : imageColor;

            updateGlobalSymptomList();
        }

        public Client() { //Completely new client (dynamically generated)
            //invManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
            //clManager = GameObject.Find("ClientManager");

            symptoms = generateSymptoms();
            harmfulProperties = generateAlergies();

            string nameGenObj = UnityEngine.Random.Range(0, 2) == 0 ? "ClientMaleNameGen" : "ClientFemaleNameGen"; //Randomize male/female names

            name = GameObject.Find(nameGenObj).GetComponent<Lexic.NameGenerator>().GetNextRandomName();

            clientColor[id] = Color.HSVToRGB(UnityEngine.Random.Range(0f, 1f), 0.35f, 0.75f);

            updateGlobalSymptomList();

            //Debug.Log(this);
        }

        public void giveMedicine() { //Returns true if cure is succesful
            Transform medicineSlot = GameObject.Find("ClientContainer").transform.Find("MedicineSlot");

            if (medicineSlot.childCount > 0) //Check if an item is inside the medicine slot
            {
                medicineItem = medicineSlot.GetChild(0).GetComponent<InventoryItemData>().itemData;

                inventory.incrementItemID(medicineItem.id, count: -1); //Remove an item

                if (medicineItem == null) return;

                if (medicineIsCompatible(medicineItem)) {
                    patientCure();

                    return;
                }

                patientKill();
            }

            //return false;
        }

        public static PropertyList generateSymptoms() {
            int difficulty = LevelManager.difficulty;
            PropertyList availableProperties = new PropertyList(Inventory.availablePropertyIDs);

            PropertyList generatedProperties = availableProperties.selectRandomCompatibleElements(UnityEngine.Random.Range(Mathf.Clamp(difficulty - 1, 1, difficulty), difficulty + 1));

            return generatedProperties;
        }

        public PropertyList generateAlergies() {
            int difficulty = LevelManager.difficulty;

            int alergyCount = UnityEngine.Random.Range(1, difficulty);

            PropertyList availableProperties = new PropertyList(Inventory.availablePropertyIDs);

            foreach (Property checkSymptom in symptoms)
            {
                List<CraftingManager.Recipe> possibleRecipes = CraftingManager.SearchRecipesForPropertyID(checkSymptom.id);

                bool hasSimpleMedicineResult = false;

                foreach (CraftingManager.Recipe checkRecipe in possibleRecipes) {
                    if (checkRecipe.itemResult.properties.count == 1) {
                        hasSimpleMedicineResult = true;
                        break;
                    }
                }

                if (hasSimpleMedicineResult) {
                    continue;
                }

                bool Continue = false;

                //Check if symptom has only 1 possible recipe, if so, then remove other symptom from possible harmful properties
                if (possibleRecipes.Count == 1) {
                    foreach (Property checkProperty in possibleRecipes[0].itemResult.properties) {
                        if (checkProperty.id != checkSymptom.id) { //If it's not the check symptom
                            availableProperties.removePropertyByID(checkProperty.id);
                            Continue = true;
                        }
                    }
                }

                if (Continue) continue;

                //Select one of the possible recipes to prioritize

                int recipeIndex = UnityEngine.Random.Range(0, possibleRecipes.Count);

                CraftingManager.Recipe targetRecipe = possibleRecipes[recipeIndex];

                foreach (Property checkProperty in targetRecipe.itemResult.properties) {
                    if (checkProperty.id != checkSymptom.id && availableProperties.inList(checkProperty)>=0) { //Check if it's not the check symptom, and if it's still available
                        //Debug.Log("Check property: " + checkProperty.id);
                        availableProperties.removePropertyByID(checkProperty.id);
                    }
                }
            }

            PropertyList alergies = symptoms.selectRandomPropertiesNotInPropertyList(availableProperties, alergyCount);

            return alergies;
        }

        public bool itemHasHarmfulProperty(Item medicine) {
            foreach (Property harmfulProperty in harmfulProperties) {
                if (medicine.properties.inList(harmfulProperty) >= 0) {
                    return true;
                }
            }

            return false;
        }

        public int itemCompatiblePropertyCount(Item medicine) {
            int count = 0;

            foreach (Property symptom in symptoms) {
                if (medicine.properties.inList(symptom)>=0) {
                    count++;
                }
            }

            return count;
        }

        public bool medicineIsCompatible(Item medicine) {
            if (GameObject.Find("ClientManager").GetComponent<ClientManager>().strictMedicineCheck)
            {
                if (symptoms.EqualTo(medicine.properties))
                {
                    //patientCure();

                    return true;
                    //return true;
                }
            }
            else
            {
                if (!itemHasHarmfulProperty(medicine))
                {
                    bool propertiesFit = true;

                    foreach (Property patientSymptom in symptoms)
                    { //Check if every symptom is in given medicine
                        if (medicine.properties.properties.FindIndex(checkProperty => checkProperty.id == patientSymptom.id) == -1)
                        {
                            //Debug.Log(patientSymptom);
                            propertiesFit = false;
                            break;
                        }
                    }

                    if (propertiesFit)
                    { //Medicine fits
                        return true;
                    }
                }
            }

            return false;
        }

        protected void patientCure()
        {
            Debug.Log("Cure succesful");

            clients.removeClient(id);

            if (medicineItem.count > 0) InventoryRender.renderItem(medicineItem.id);

            OnClientCured.Invoke(id);
        }

        protected void patientKill() {
            Debug.Log("Patient died. woops");

            OnClientDied?.Invoke(id);

            LevelManager.patientDeaths++;

            if (medicineItem.count > 0) InventoryRender.renderItem(medicineItem.id);

            clients.removeClient(id);
        }

        void updateGlobalSymptomList() {
            foreach (Property checkSymptom in symptoms) {
                int symptomIndex = currentPatientSymptoms.FindIndex(symptom => symptom["id"] == checkSymptom.id);

                if (symptomIndex < 0) { //Check if symptom exists in list
                    currentPatientSymptoms.Add(new Dictionary<string, int>() {
                        { "id", checkSymptom.id},
                        { "count", 1}
                    });
                }
                else {
                    currentPatientSymptoms[symptomIndex]["count"] += 1; //If the symptom exists, increase the count by one
                }
            }
        }

        public override string ToString()
        {
            return $"[ID: {id}, Name: {name}, Symptoms: {symptoms}, Avoid: {harmfulProperties}]";
        }
    }

    public class ClientList : IEnumerable<Client>{
        public List<Client> clientList = new List<Client>();
        public int Count => clientList.Count;

        public ClientList(){
            //Initialize ClientList

            //GameObject[] gameObjects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
            //GameObject clContainer = gameObjects.SingleOrDefault(obj => obj.name == "ClientContainer");
        }

        public int addClient(Client client) { //Returns the ID of newly added client
            //Add client to internal list
            client.id = clientList.Count;
            clientList.Add(client);

            //Render client
            //GameObject clientObj = clRenderer.renderClient(client);
            //clientObj.transform.Find("SubmitBtn").GetComponent<Button>().onClick.AddListener(client.giveMedicine);

            OnClientNew?.Invoke(client.id);

            return client.id;
        }

        public void removeClient(int id) {
            int clientIndex = clientList.FindIndex(client => client.id == id);
            Destroy(clientNoteObj[clientList[clientIndex].id]); //Destroy the patient board note object for this client

            clientList.RemoveAt(clientIndex);
            ClientRenderer.removeCurrentClient();

            if (clientList.Count == 0) {
                OnClientsEmpty?.Invoke();
            }
        }

        public void removeAllClients() {
            foreach (Client client in clientList.ToList()) {
                Destroy(clientNoteObj[client.id]);
                clientList.Remove(client);
            }

            currentPatientSymptoms = new List<Dictionary<string, int>>();
        }

        public Client getClient(int index) {
            return clientList[index];
        }

        public int getClientIndex(Client checkClient) {
            return clientList.FindIndex(client => client.id == checkClient.id);
        }

        public void printClients() {
            Debug.Log("Client readout: ");
            foreach (Client el in clientList)
            {
                Debug.Log("ID: " + el.id + "; Name: " + el.name + "; Symptoms: " + el.symptoms);
            }
            Debug.Log("-------------------");
        }

        #region Indexer definition
        public Client this[int index]
        { //Access indexer
            get
            {
                if (index < 0)
                {
                    throw new IndexOutOfRangeException("Index out of range");
                }

                return clientList[index];
            }
            set
            {
                if (index < 0 || index > clientList.Count - 1)
                {
                    throw new IndexOutOfRangeException("Index out of range");
                }

                clientList[index] = value;
            }
        }
        #endregion

        #region IEnumerator definition
        public IEnumerator<Client> GetEnumerator() => clientList.GetEnumerator();

        //IEnumerable Members
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    void Start() {
        //clients = new ClientList();

        //OnManagerInit?.Invoke();
    }

    public delegate void ManagerInit();
    public static event ManagerInit OnManagerInit;
}
