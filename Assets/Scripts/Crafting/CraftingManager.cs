﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using static InventoryManager;

public class CraftingManager : MonoBehaviour
{
    public static List<Recipe> recipeList = new List<Recipe>();
    private float internalTimer;
    public bool currentlyCrafting { get; private set; } = false;
    public float craftingLength = 1; //How many seconds is the crafting process

    public static List<Recipe> discoveredRecipes = new List<Recipe>();

    //Events

    public delegate void CraftingStart();
    public static event CraftingStart OnCraftingStart;

    public delegate void CraftingEnd();
    public static event CraftingEnd OnCraftingEnd;

    public delegate void CraftingFail();
    public static event CraftingFail OnCraftingFail;

    GameObject craftingBtnOverlay;

    [System.Serializable]
    public class Recipe{
        public List<Item> ingredientList { get; private set; }
        public Item itemResult { get; private set; }
        public PropertyList requiredProperties; // A variable used for most efficient crafting route calculations

        public Recipe(Item inA, Item inB, Item inResult) {
            ingredientList = new List<Item>() {inA, inB };
            itemResult = inResult;

            requiredProperties = new PropertyList();
        }

        public bool IngredientsEqual(Recipe checkRecipe) {
            return ingredientList.Except(checkRecipe.ingredientList).ToList().Count == 0;
        }

        public bool IngredientsEqual(List<Item> checkIngredients)
        {
            return ingredientList.Except(checkIngredients).ToList().Count == 0;
        }

        public bool requiresItemWithID(int itemID) {
            return ingredientList.Contains(inventory.getItemByID(itemID));
        }

        public override string ToString() {
            string output = "Ingredients: ";

            for (int i = 0; i < ingredientList.Count; i++) {
                output += ingredientList[i].id;
                if (i != ingredientList.Count) output += ", ";
            }

            return output + "Result: " + itemResult.id;
        }
    }

    public static List<Recipe> SearchRecipesForPropertyID(int targetPropertyID)
    {
        //Find recipes, where the result has the desired property

        List<Recipe> targetRecipes;

        targetRecipes = recipeList.FindAll(recipe => recipe.itemResult.hasPropertyWithID(targetPropertyID));

        //Find recipes, for which all the ingredients are unlocked

        foreach (Recipe recipe in targetRecipes.ToList()) {
            foreach (Item ingredient in recipe.ingredientList) {
                if (!Inventory.unlockedBaseIngredients.Contains(ingredient.id)) {
                    targetRecipes.Remove(recipe); //Remove potential recipe from list
                    break; //Break the loop, which checks the ingredients
                }
            }
        }

        return targetRecipes;
    }

    public static List<Recipe> FindRecipesWithItemID( int itemID, bool bothIngredientsUnlocked = true) {
        List<Recipe> potentialRecipes = recipeList.FindAll(recipe => recipe.requiresItemWithID(itemID));

        if (bothIngredientsUnlocked) { //Check ingredients, only if both need to be unlocked
            foreach (Recipe recipe in potentialRecipes.ToList())
            {
                foreach (Item ingredient in recipe.ingredientList)
                {
                    if (Inventory.unlockedBaseIngredients.FindIndex(id => id == ingredient.id)<0)
                    {
                        potentialRecipes.Remove(recipe); //If any item is locked, remove it from potential recipes
                        break; //Break the ingredient check loop
                    }
                }
            }
        }

        return potentialRecipes;
    }

    public void startCrafting() {
        // Check if both slots have items
        GameObject crafterSlotA = GameObject.Find("CrafterSlotA");
        GameObject crafterSlotB = GameObject.Find("CrafterSlotB");
        //InventoryManager invComp = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();

        if (crafterSlotA.transform.childCount != 0 && crafterSlotB.transform.childCount != 0)
        {
            currentlyCrafting = true;

            Item itemA = crafterSlotA.transform.GetChild(0).GetComponent<InventoryItemData>().itemData;
            Item itemB = crafterSlotB.transform.GetChild(0).GetComponent<InventoryItemData>().itemData;

            inventory.lockItemID(itemA.id, returnToInventory: false);
            inventory.lockItemID(itemB.id, returnToInventory: false);

            craftingBtnOverlay.SetActive(true);
            craftingBtnOverlay.GetComponent<Animator>().SetBool("crafterWorking", true);

            OnCraftingStart?.Invoke();
        }
    }

    void craftItem() { //Returns true if crafting succesful
        // Check if both slots have items
        GameObject crafterSlotA = GameObject.Find("CrafterSlotA");
        GameObject crafterSlotB = GameObject.Find("CrafterSlotB");
        //InventoryManager invComp = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();

        if (crafterSlotA.transform.childCount!=0 && crafterSlotB.transform.childCount!=0) {
            //Get both items
            Item itemA = crafterSlotA.transform.GetChild(0).GetComponent<InventoryItemData>().itemData;
            Item itemB = crafterSlotB.transform.GetChild(0).GetComponent<InventoryItemData>().itemData;

            GameObject resultSlot = GameObject.Find("CrafterSlotResult");

            //Check if recipe exists with selected crafting ingredients
            int recipeIndex = recipeList.FindIndex(checkRecipe => checkRecipe.IngredientsEqual(new List<Item>() { itemA, itemB }));

            if (recipeIndex != -1) //Combine base ingredients
            {
                Recipe selectedRecipe = recipeList[recipeIndex];
                Item itemResult = selectedRecipe.itemResult;

                if (inventory.getItemByID(itemResult.id) == null) //Check if item exists in inventory
                { //Item doesnt exist
                    discoveredRecipes.Add(selectedRecipe);

                    Debug.Log("Item doesnt exist yet, add new recipe");

                    //Add new item to inventory
                    inventory.addItem(itemResult.Clone()); //Add a clone of the itemResult to the inventory

                    //Render new item inside result slot
                    InventoryRender.renderItem(itemResult.id, resultSlot, true);
                }
                else
                { //Item already exists
                    inventory.incrementItemID(itemResult.id);

                    GameObject itemObj = Item.itemGameObjects[itemResult.id];

                    itemObj.GetComponent<InventoryDragHandler>().inSlot = true;
                    itemObj.transform.position = resultSlot.transform.position;
                    itemObj.transform.SetParent(resultSlot.transform);
                    itemObj.GetComponent<LayoutElement>().ignoreLayout = true;
                }
            }
            else if (itemA.properties.count > 0 && itemB.properties.count > 0)
            {
                //Combine item properties
                PropertyList resultProperties = new PropertyList(itemA.properties, itemB.properties);

                int existingItemID = inventory.findItemWithProperties(resultProperties);

                if (existingItemID != -1) // If item already exists, increment its count and move the item gameobject to the result slot
                {
                    Debug.Log("Item exists");

                    inventory.incrementItemID(existingItemID);

                    GameObject itemObj = Item.itemGameObjects[existingItemID];

                    itemObj.GetComponent<InventoryDragHandler>().inSlot = true;
                    itemObj.transform.position = resultSlot.transform.position;
                    itemObj.transform.SetParent(resultSlot.transform);
                    itemObj.GetComponent<LayoutElement>().ignoreLayout = true;
                }
                else
                { //If item doesn't exist already, create a new one
                  //Create new item
                    Item itemResult = new Item(initProperties: resultProperties, initCount: 1);

                    //Add new item to inventory
                    inventory.addItem(itemResult);

                    //Render new item inside result slot
                    InventoryRender.renderItem(itemResult.id, resultSlot, true);
                }
            }
            else {
                Debug.Log("crafting fail");
                OnCraftingFail?.Invoke();
            }

            //subtract one of each ingredient
            inventory.incrementItemID(itemA.id, -1); 
            inventory.incrementItemID(itemB.id, -1);

            LevelManager.ingredientsUsed += 2; //Increase the amount of ingredients used in the current level

            //InventoryRender.updateAllItems();

            //return true;
        }

        //return false;
    }

    void OnCraftingMenuClose() {
        if (currentlyCrafting)
        {
            currentlyCrafting = false;
            internalTimer = 0;
            craftingBtnOverlay.GetComponent<Animator>().SetBool("crafterWorking", false);
            craftingBtnOverlay.SetActive(false);

            Transform crafterMenuTransform = MenuManager.crafterMenu.transform;

            Transform crafterSlotA = crafterMenuTransform.Find("CrafterSlotA");
            Transform crafterSlotB = crafterMenuTransform.Find("CrafterSlotB");

            if (crafterSlotA.childCount > 0) {
                GameObject itemObj = crafterSlotA.GetChild(0).gameObject;
                inventory.lockItemID(itemObj.GetComponent<InventoryItemData>().itemData.id, locked: false);
                //InventoryRender.renderItem(itemObj.GetComponent<InventoryItemData>().itemData.id);
                Destroy(itemObj);
            }

            if (crafterSlotB.childCount > 0)
            {
                GameObject itemObj = crafterSlotB.GetChild(0).gameObject;
                inventory.lockItemID(itemObj.GetComponent<InventoryItemData>().itemData.id, locked: false);
                //InventoryRender.renderItem(itemObj.GetComponent<InventoryItemData>().itemData.id);
                Destroy(itemObj);
            }
        }
    }

    void Start() {
        GameObject[] gameObjects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
        craftingBtnOverlay = gameObjects.SingleOrDefault(obj => obj.name == "WorkingOverlay");

        MenuManager.OnMenuClose += OnCraftingMenuClose;
    }

    private void Update()
    {
        if (currentlyCrafting) {
            //Update crafting timer

            internalTimer += Time.deltaTime;

            if (internalTimer >= craftingLength) { //Check if craftingLength (s) has past
                craftItem();

                currentlyCrafting = false;
                craftingBtnOverlay.GetComponent<Animator>().SetBool("crafterWorking", false);
                craftingBtnOverlay.SetActive(false);

                OnCraftingEnd?.Invoke();

                internalTimer = 0;
            }
        }
    }
}
