﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lexic {
    public class MedicineNameDictionary : BaseNames {
        private static Dictionary<string, List<string>> syllableSets = new Dictionary<string, List<string>>()
            {
                {
                    "prefix",    new List<string>(){
                        "Ibu", "Para", "Meta", "Hydro", "Lora", "Di", "Oxy", "Tri", "Acetyl"
                    }
                },
                {
                    "premid", new List<string>(){
                        "phen", "tor"
                    }
                },
                {
                    "mid", new List<string>(){
                        "cet", "prof", "phet", "cod", "hydr", "met"
                    }
                },
                {
                    "postmid", new List<string>(){
                        "astat"
                    }
                },
                {
                    "suffix", new List<string>(){
                        "ol", "avir", "amine", "one", "adine", "in", "acin", "oline"
                    }
                }
            };

        private static List<string> rules = new List<string>()
            {
                "%100prefix%15premid%100mid%33postmid%100suffix"
            };

        public new static List<string> GetSyllableSet(string key) { return syllableSets[key]; }

        public new static List<string> GetRules() { return rules; }
    }
}