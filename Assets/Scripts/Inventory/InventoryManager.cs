﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using static Extensions;

public class InventoryManager : MonoBehaviour
{
    public bool checkForIncompatibleProperties = false;

    [Serializable]
    public class Property : IComparable
    {
        public int id;
        public string name
        {
            get { return PropertyList.nameList[id]; }
        }
        //public int rarityFactor;
        public List<int> incompatibleIDs => PropertyList.propertyIncompatList[id];

        public Property(int id_in)
        {
            id = id_in;
        }

        public bool isIncompatible(int checkPropertyID) {
            return incompatibleIDs.Contains(checkPropertyID);
        }

        #region ToString override
        public override string ToString()
        {
            string incompatIDs = incompatibleIDs.Count == 0 ? "None" : String.Join(", ", incompatibleIDs);
            return "{ID: " + id + ", Name: " + name + ", Incompatible Property IDs: " + incompatIDs + "}";
        }
        #endregion

        #region IComparable definition
        int IComparable.CompareTo(object obj)
        {
            Property prop = (Property)obj;
            return this.id.CompareTo(prop.id);

        }
        #endregion
    }

    [Serializable]
    public class PropertyList : IEnumerable<Property> { //Property list wrapper
        static public string[] nameList = { //Property names for the specific property ID, array index is property ID
            "constipation",
            "cough",
            "diarrhea",
            //"wheezing",
            "sore throat",
            "numbness",
            "vomiting",
            "nasal congestion",
            "headache",
            "difficulty swallowing",
            //"nausea",
            //"skin rash",
            "fever",
            "confusion",
            "infection",
            "pain"
        };

        static public List<List<int>> propertyIncompatList = new List<List<int>>(){ //List index is property id
            new List<int>(){ 1}, //Incompatibility with property ID 1
            new List<int>(){ 0}, //Incompatibility with property ID 0
            new List<int>(){}, 
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){},
            new List<int>(){}
        };

        public List<Property> properties { get; private set; } = new List<Property>();
        public int count => properties.Count;

        //Initialize PropertyList object with an array of the containing property IDs or don't enter the parameter for empty PropertyList
        public PropertyList(int[] newPropertyIDs = null) {
            if (newPropertyIDs != null) {
                foreach (int id in newPropertyIDs)
                {
                    properties.Add(new Property(id)); //Add all the new properties to the internal property list
                }
            }
        }

        public PropertyList(List<int> newPropertyIDs)
        {
            if (newPropertyIDs != null)
            {
                foreach (int id in newPropertyIDs)
                {
                    properties.Add(new Property(id)); //Add all the new properties to the internal property list
                }

                properties = properties.Distinct().ToList();
            }
        }

        public PropertyList(PropertyList listA, PropertyList listB) {
            properties.AddRange(listA);
            properties.AddRange(listB);

            if (GameObject.Find("InventoryManager").GetComponent<InventoryManager>().checkForIncompatibleProperties) {
                //Check for incompatible properties (optimizations?)
                for (int checkPropID = 0; checkPropID < listA.count; checkPropID++)
                {
                    foreach (int incompatibleID in listA[checkPropID].incompatibleIDs)
                    {
                        int searchIndex = listB.inList(incompatibleID);
                        if (searchIndex >= 0)
                        {
                            properties.RemoveAt(checkPropID);
                            properties.RemoveAt(listA.count + searchIndex - 1); //-1 to compensate for the removing of the listA property
                        }
                    }
                }
            }

            properties = properties.Distinct().ToList();
        }

        public PropertyList(List<Property> inProperties) {
            properties = inProperties;
        }

        public int inList(Property checkProperty) {
            return properties.BinarySearch(checkProperty);
        }

        public int inList(int checkPropertyID)
        {
            return properties.BinarySearch(new Property(checkPropertyID));
        }

        public string ToStringList() {
            string output = "";

            foreach (Property property in properties) {
                output += "• " + UppercaseFirst(property.name) + "\n";
            }

            return output;
        }

        public List<int> getPropertyIDs() {
            List<int> output = new List<int>();

            foreach (Property Property in properties) {
                output.Add(Property.id);
            }

            return output;
        }

        public bool EqualTo(PropertyList compareList) {
            if (properties.Count == compareList.count) {
                foreach (Property property in compareList) {
                    if (properties.FindIndex(checkProperty => checkProperty.id == property.id)==-1) {
                        return false;
                    } 
                }

                return true;
            }

            return false;
        }

        public int addProperty(Property newProperty) {
            properties.Add(newProperty);

            properties = new PropertyList(getPropertyIDs().Distinct().ToList()).properties; //janky af

            return properties.Count - 1; //Return new id
        }

        public void removeProperty(int propertyIndex) {
            properties.RemoveAt(propertyIndex);
        }

        public void removePropertyByID(int propertyID) {
            int index = properties.FindIndex(property => property.id == propertyID);

            if (index == -1) {
                Debug.Log("Property not found: " + propertyID);
                Debug.Log(ToString());
            }
            //Debug.Log("Remove at: ");
            //Debug.Log(index);

            removeProperty(index);
        }

        public PropertyList selectRandomCompatibleElements(int neededCount) {
            List<Property> output = new List<Property>();
            List<Property> tempList = new List<Property>();

            tempList.AddRange(properties);

            for (int i = 0; i < (neededCount <= tempList.Count ? neededCount : tempList.Count); i++)
            {
                int listIndex = UnityEngine.Random.Range(0, tempList.Count);

                //Debug.Log("Temp list count: " + tempList.Count);
                //Debug.Log("Index: " + listIndex);

                if (!propertyIsIncompatibleInList(tempList[listIndex].id, output))
                {
                    output.Add(tempList[listIndex]);
                }
                else
                {
                    i--;
                }

                tempList.RemoveAt(listIndex);
            }

            return new PropertyList(output);
        }

        public PropertyList selectRandomPropertiesNotInPropertyList(PropertyList checkList, int neededCount) {
            //Select properties from PropertyList checkList, if they're not in parent PropertyList

            PropertyList output = new PropertyList();
            PropertyList tempList = new PropertyList(checkList.getPropertyIDs().Except(getPropertyIDs()).ToList()); //Remove properties from parent PropertyList, that are in PropertyList checkList

            for (int i = 0; i < neededCount; i++)
            {
                if (tempList.count == 0) return output;

                int listIndex = UnityEngine.Random.Range(0, tempList.count - 1);
                output.addProperty(tempList[listIndex]);
                tempList.removeProperty(listIndex);
            }

            return output;
        }

        public bool propertyIsIncompatibleInList(int id, List<Property> checkList) {
            if (checkList.Count == 0) return false;

            foreach (Property checkProperty in checkList) {
                bool output = checkProperty.isIncompatible(id);
                if (output) return true;
            }

            return false;
        }

        #region ToString override
        public override string ToString()
        {
            string output = "{Count: "+count+"; Properties: \n";

            foreach (Property property in properties) {
                output += property.ToString() + ", \n";
            }

            output += "}";

            return output;
        }
        #endregion

        #region Indexer definition
        public Property this[int index]
        { //Access indexer
            get
            {
                if (index < 0)
                {
                    throw new IndexOutOfRangeException("Index out of range");
                }

                return properties[index];
            }
            set
            {
                if (index < 0 || index > properties.Count - 1)
                {
                    throw new IndexOutOfRangeException("Index out of range");
                }

                properties[index] = value;
            }
        }
        #endregion

        #region IEnumerator definition
        public IEnumerator<Property> GetEnumerator() => properties.GetEnumerator();

        //IEnumerable Members
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    [Serializable]
    public class Item
    {
        static public List<string> nameList = new List<string>() {}; //Item names for hardcoded items (Base ingredients)

        static public int lastGlobalItemID = nameList.Count-1; //The item id of the last added item; last hardcoded item ID as default value

        public static Dictionary<int, GameObject> itemGameObjects = new Dictionary<int, GameObject>();
        public static Dictionary<int, Sprite> itemSprites = new Dictionary<int, Sprite>();

        public int id { get; private set; }
        public int count;
        //public GameObject gameObject;
        public bool isBaseIngredient; //If the item is a base ingredient or medication
        public bool locked; //If the item is locked in inventory
        public string description;
        //public Sprite sprite;

        public string name => getName(); 
        private string customName = "";

        public PropertyList properties { get; private set; }

        public Item(int itemID = -1, bool baseIngredient = false, bool startLocked = false, int initCount = 1, PropertyList initProperties = null, string initDescription = "", string presetName = "") {
            //If no item ID is given, then a new item is created

            //Initialize item

            if (itemID == -1)
            {
                //Generate new name

                Lexic.NameGenerator nameGen = GameObject.Find("MedicineNameGen").GetComponent<Lexic.NameGenerator>();

                string candidateName = "";

                while (candidateName == "" || nameList.Contains(candidateName))
                { //Generate new name, until a unique name is generated
                    candidateName = nameGen.GetNextRandomName();
                }

                nameList.Add(candidateName);
            }

            if (presetName != "") customName = presetName;

            count = initCount;
            id = itemID == -1 ? ++lastGlobalItemID : itemID; //If no id given, then create a new item id and set it as the item id
            isBaseIngredient = baseIngredient;
            locked = startLocked;
            description = initDescription;

            if (initProperties != null)
            {
                properties = initProperties;
            }
            else {
                properties = new PropertyList();
            }
        }

        private string getName()
        {
            if (customName != "") return customName;

            return nameList.Count - 1 < id ? "Placeholder" : nameList[id]; //isBaseIngredient ? nameList[id] : "Placeholder";
        }

        public void setProperties(PropertyList newProperties) {
            properties = newProperties;
        }

        public bool hasPropertyWithID(int checkID) {
            return !(properties.getPropertyIDs().FindIndex(index=>index==checkID)<0);
        }

        public override string ToString()
        {
            return $"ID: {id}, name: {name}, properties: {properties}, count: {count}, Base ingredient: {isBaseIngredient}, description: {description}, Locked: {locked}";
        }

        public Item Clone() {
            return new Item(
                itemID: id,
                initProperties: properties,
                presetName: name,
                baseIngredient: isBaseIngredient,
                initDescription: description
            );
        }
    }

    public class Inventory {
        public List<Item> items = new List<Item>();

        public static List<int> availablePropertyIDs = new List<int>(); //A list of the available property IDs
        public static List<int> unlockedBaseIngredients = new List<int>();
        public static List<int> medicineItemIDs = new List<int>();

        public int Count => items.Count;

        public Inventory() {
            //Initialize inventory 
        }

        public int addItem(Item newItem) { //Add new item
            items.Add(newItem);

            if (newItem.properties != null && !newItem.locked) {
                if (newItem.isBaseIngredient)
                {
                    updateBaseIngredients(newItem);
                }
                else {
                    medicineItemIDs.Add(newItem.id);
                }

                //Keep track of any new properties obtained by player
                foreach (Property checkProperty in newItem.properties)
                {
                    if (!availablePropertyIDs.Contains(checkProperty.id))
                    {
                        availablePropertyIDs.Add(checkProperty.id); //Add new property id to the list
                    }
                }
            }

            return items.Count-1; //Return the slot of the new item
        }

        public void updateBaseIngredients(Item newItem) {
            unlockedBaseIngredients.Add(newItem.id);
        }

        public void removeItemAtSlot(int slot) { //Remove item at slot
            if (medicineItemIDs.Contains(items[slot].id)) {
                medicineItemIDs.Remove(items[slot].id);
            }

            items.RemoveAt(slot);
        }

        public void removeItemByID(int id) {
            if (medicineItemIDs.Contains(id))
            {
                medicineItemIDs.Remove(id);
            }

            items.Remove(getItemByID(id));
        }

        public int getItemSlot(int id) {
            return items.FindIndex(i => i.id == id);
        }

        public Item getItemBySlot(int slot) { 
            return items[slot];
        }

        public Item getItemByID(int id) {
            return items.Find(i => i.id == id);
        }

        private int incrementItem(Item targetItem, int count)
        {
            targetItem.count += count;

            if (targetItem.count <= 0)
            {
                if (targetItem.count < 0) targetItem.count = 0; //Clamp the item's min value to 0

                lockItemID(targetItem.id);
            }
            else if (targetItem.locked) lockItemID(targetItem.id, locked: false); //Unlock item if incremented above 0

            InventoryRender.updateItem(targetItem.id); //Update the item game object

            return targetItem.count; //Return new item count
        }

        public int incrementItemSlot(int slot, int count = 1) {
            Item targetItem = getItemBySlot(slot);

            return incrementItem(targetItem, count);
        }

        public int incrementItemID(int id, int count = 1) {
            Item targetItem = getItemByID(id);

            return incrementItem(targetItem, count);
        }

        public void setAmountItemSlot(int slot, int count) {
            Item targetItem = getItemBySlot(slot);

            targetItem.count = count;
            InventoryRender.updateItem(targetItem.id);
        }

        public void setAmountItemID(int id, int count = 1)
        {
            Item targetItem = getItemByID(id);

            targetItem.count = count;
            InventoryRender.updateItem(targetItem.id);
        }

        public void changeItemSlot(int oldSlot, int newSlot) {
            Item temp = items[oldSlot]; //Save old variable in temp
            items.RemoveAt(oldSlot);
            items.Insert(newSlot, temp);
        }

        public void changeItemSlotByID(int id, int newSlot)
        {
            Item temp = getItemByID(id); //Save old variable in temp
            items.Remove(temp);
            items.Insert(newSlot, temp);
        }

        public void lockItemID(int id, bool locked = true, bool returnToInventory = true) { //Lock item
            Item targetItem = getItemByID(id);
            targetItem.locked = locked;

            if (targetItem.isBaseIngredient&&!locked) {
                unlockedBaseIngredients.Add(targetItem.id);
            }
            
            InventoryRender.lockItem(id, locked, returnToInventory);
        }

        public int findItemWithProperties(PropertyList checkProperties) { //Check if item with specified properties already exists
            Item search = items.Find(i => ScrambledEquals(i.properties, checkProperties));
            return search==null ? -1 : search.id; //Return id of item with specified properties
        }

        public void clearMedicineItems() {
            foreach (int id in medicineItemIDs) {
                int index = getItemSlot(id);
                Destroy(Item.itemGameObjects[items[index].id]); //Destroy item object

                //Reset to default values
                items[index].locked = false;
                items[index].count = 1;

                items.RemoveAt(index); //Remove from item list
            }

            medicineItemIDs.RemoveAll(id => true); //Remove all elements
        }

        public void printInventory() {
            Debug.Log("Inventory readout: ");
            foreach (Item el in items) {
                Debug.Log("ID: "+el.id+"; Name: "+el.name+"; Count: "+el.count);
            }
            Debug.Log("-------------------");
        }

        public void printUnlockedProperties() {
            string output = "";
            foreach (int id in availablePropertyIDs) output += id.ToString()+" ";
            Debug.Log("Unlocked property IDs: " + output);
        }

        public void clearInventory() {
            foreach (Item item in items) { //Destroy all item objects
                Destroy(Item.itemGameObjects[item.id]); 
            }

            items.RemoveAll(item => true); //Remove all elements
            medicineItemIDs.RemoveAll(item => true); //Remove all medicine items
            availablePropertyIDs.RemoveAll(property => true); //Remove all available properties
            unlockedBaseIngredients.RemoveAll(item => true); //Remove all unlocked base ingredients
        }
    }

    public static Inventory inventory;// = new Inventory(); //Initialize the inventory

    static public GameObject getActiveInventoryMenu()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("InventoryMenu"))
        {
            if (obj.activeSelf == true)
            {
                return obj;
            }
        }

        return new GameObject();
    }

    void Start()
    {
        if (inventory == null) inventory = new Inventory();

        if (!checkForIncompatibleProperties) {
            for (int i = 0; i < PropertyList.propertyIncompatList.Count; i++) {
                PropertyList.propertyIncompatList[0] = new List<int>();
            }
        }
    }
}
