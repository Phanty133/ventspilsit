﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static InventoryManager;

public class InventoryRender : MonoBehaviour
{
    public int renderMargin = 50;
    public int containerPadding = 30;
    public GameObject itemTemplate;
    public GameObject descriptionTemplate;
    public Sprite waterSprite;
    public Sprite[] itemSprites; //Item sprites

    static private GameObject createItem(Item item, GameObject parent, bool duplicateItem = false) {
        InventoryRender invRender = GameObject.FindGameObjectWithTag("PlayerInventoryParent").GetComponent<InventoryRender>();
        GameObject itemTemplate = invRender.itemTemplate;
        Sprite[] itemSprites = invRender.itemSprites;

        GameObject itemObj = Instantiate(itemTemplate, parent.transform);

        if(!duplicateItem) Item.itemGameObjects[item.id] = itemObj;

        Transform itemSprite = itemObj.transform.Find("ItemSpriteContainer").Find("ItemSprite");
        Transform itemName = itemObj.transform.GetChild(1);
        Transform itemCount = itemObj.transform.Find("ItemSpriteContainer").Find("ItemCount");

        itemObj.GetComponent<InventoryItemData>().itemData = item;
        itemName.GetComponent<Text>().text = item.name;
        itemCount.GetComponent<Text>().text = item.count.ToString();

        if (item.locked) itemObj.transform.Find("lockedOverlay").gameObject.SetActive(true); //Lock item, if initialized as locked
        if (item.isBaseIngredient) itemObj.GetComponent<Image>().color = new Color(0.25f,0.6f,1f, 1);

        //Update item description component

        TooltipReceiver tooltip = itemObj.GetComponent<TooltipReceiver>();

        tooltip.tooltipTitle = item.name + "\n" + (item.isBaseIngredient ? "Description" : "Properties");

        if (item.isBaseIngredient)
        {
            tooltip.tooltipContent = item.description;
        }
        else {
            foreach (Property property in item.properties)
            {
                //Add each property to tooltip
                //Append property name, append new line before property, if its not the first line
                tooltip.tooltipContent += (tooltip.tooltipContent == "" ? "" : "\n") + "Cures " + property.name;
            }
        }

        if (!Item.itemSprites.ContainsKey(item.id)) Item.itemSprites[item.id] = null;

        if (item.id == 8)
        { //If the item is water
            itemSprite.GetComponent<Image>().sprite = invRender.waterSprite;
        }
        else {
            if (Item.itemSprites[item.id] == null) Item.itemSprites[item.id] = itemSprites[Random.Range(0, itemSprites.Length)];

            itemSprite.GetComponent<Image>().sprite = Item.itemSprites[item.id];
        }

        return itemObj;
    }

    static public void renderItems()
    {
        //InventoryManager invComp = GameObject.Find("InventoryManager").GetComponent<InventoryManager>(); //Include inventory tracker component from InventoryManager
        GameObject[] invObj = GameObject.FindGameObjectsWithTag("PlayerInventory");

        foreach (GameObject container in invObj)
        {
            if (container.activeSelf)
            { //Don't render items, if the inventory is not being displayed
                foreach (Transform child in container.transform) { Destroy(child.gameObject); } //Destroy currently rendered items (TODO: destroy only items, that have changed)

                foreach (Item item in inventory.items) {
                    createItem(item, container);
                }
            }
        }
    }

    static public void renderItem(int itemID, GameObject parent = null, bool inSlot = false) {
        //InventoryManager invComp = GameObject.Find("InventoryManager").GetComponent<InventoryManager>(); //Include inventory tracker component from InventoryManager

        if (parent == null) { //Render in inventory
            parent = GameObject.Find("InvContainer");
        }

        Item targetItem = inventory.getItemByID(itemID);
        GameObject itemObj = createItem(targetItem, parent);
        itemObj.GetComponent<InventoryDragHandler>().inSlot = inSlot;

        if(parent!=null) itemObj.transform.position = parent.transform.position;
    }

    static public GameObject renderDuplicateItem(Item targetItem, GameObject parent) {
        return createItem(targetItem, parent, duplicateItem: true);
    }

    static public void updateAllItems() {
        //InventoryManager invComp = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();

        foreach (GameObject itemObj in GameObject.FindGameObjectsWithTag("InventoryItem")) {
            InventoryItemData itemDataComp = itemObj.GetComponent<InventoryItemData>();
            int itemID = itemDataComp.itemData.id;
            itemDataComp.itemData = inventory.getItemByID(itemID);

            Transform itemSprite = itemObj.transform.GetChild(0);
            Transform itemCount = itemObj.transform.Find("ItemSpriteContainer").Find("ItemCount");

            itemCount.GetComponent<Text>().text = itemDataComp.itemData.count.ToString();
        }
    }

    static public void updateItem(int itemID) {
        //InventoryManager invComp = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
        Item targetItem = inventory.getItemByID(itemID);
        GameObject itemObj = Item.itemGameObjects[targetItem.id];
        InventoryItemData itemDataComp = itemObj.GetComponent<InventoryItemData>();

        itemDataComp.itemData = targetItem;

        //Transform itemSprite = itemObj.transform.GetChild(0);
        Transform itemCount = itemObj.transform.Find("ItemSpriteContainer").Find("ItemCount");

        itemCount.GetComponent<Text>().text = itemDataComp.itemData.count.ToString();
    }

    static public void lockItem(int itemID, bool locked = true, bool returnToInventory = true) {
        //InventoryManager invComp = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
        Item targetItem = inventory.getItemByID(itemID);
        GameObject itemObj = Item.itemGameObjects[targetItem.id];

        itemObj.transform.Find("lockedOverlay").gameObject.SetActive(locked);

        if (itemObj.GetComponent<InventoryDragHandler>().inSlot&&returnToInventory) { //Return item to inventory, if it's in a craft slot
            Transform invParent = GameObject.Find("InvContainer").transform;
            itemObj.transform.SetParent(invParent);
            itemObj.transform.SetSiblingIndex(inventory.getItemSlot(targetItem.id));
            itemObj.GetComponent<LayoutElement>().ignoreLayout = false;
        }
    }

    // Start is called before the first frame update
    void Start() {}
}
