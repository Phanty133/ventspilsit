﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalRenderer : MonoBehaviour
{
    public GameObject recipeTemplate;

    public void renderAllDiscoveredRecipes() {
        GameObject container = GameObject.Find("JournalItemContainer");

        foreach (Transform child in container.transform) //Delete all children
        {
            Destroy(child.gameObject);
        }

        foreach (CraftingManager.Recipe recipe in CraftingManager.discoveredRecipes) {
            GameObject recipeObj = Instantiate(recipeTemplate, container.transform);

            Transform itemASlot = recipeObj.transform.Find("ItemSlotA");
            Transform itemBSlot = recipeObj.transform.Find("ItemSlotB");
            Transform itemResultSlot = recipeObj.transform.Find("ItemSlotResult");

            InventoryManager.Item itemADuplicate = recipe.ingredientList[0].Clone();

            InventoryManager.Item itemBDuplicate = recipe.ingredientList[1].Clone();

            InventoryManager.Item itemResultDuplicate = recipe.itemResult.Clone();

            GameObject itemA = InventoryRender.renderDuplicateItem(itemADuplicate, parent: itemASlot.gameObject);
            itemA.GetComponent<InventoryDragHandler>().enabled = false;
            itemA.transform.localScale = new Vector2(0.5f, 0.5f);

            RectTransform itemARect = itemA.GetComponent<RectTransform>();
            itemARect.anchoredPosition = new Vector2();
            itemARect.anchorMin = new Vector2(0.5f, 0.5f);
            itemARect.anchorMax = new Vector2(0.5f, 0.5f);
            itemARect.pivot = new Vector2(0.5f, 0.5f);

            GameObject itemB = InventoryRender.renderDuplicateItem(itemBDuplicate, parent: itemBSlot.gameObject);
            itemB.GetComponent<InventoryDragHandler>().enabled = false;
            itemB.transform.localScale = new Vector2(0.5f, 0.5f);

            RectTransform itemBRect = itemB.GetComponent<RectTransform>();
            itemBRect.anchoredPosition = new Vector2();
            itemBRect.anchorMin = new Vector2(0.5f, 0.5f);
            itemBRect.anchorMax = new Vector2(0.5f, 0.5f);
            itemBRect.pivot = new Vector2(0.5f, 0.5f);

            GameObject itemResult = InventoryRender.renderDuplicateItem(itemResultDuplicate, parent: itemResultSlot.gameObject);
            itemResult.GetComponent<InventoryDragHandler>().enabled = false;
            itemResult.transform.localScale = new Vector2(0.5f, 0.5f);

            RectTransform itemResultRect = itemResult.GetComponent<RectTransform>();
            itemResultRect.anchoredPosition = new Vector2();
            itemResultRect.anchorMin = new Vector2(0.5f, 0.5f);
            itemResultRect.anchorMax = new Vector2(0.5f, 0.5f);
            itemResultRect.pivot = new Vector2(0.5f, 0.5f);
        }
    }
}
