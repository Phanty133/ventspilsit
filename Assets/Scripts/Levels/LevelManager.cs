﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static InventoryManager;
using static CraftingManager;

public class LevelManager : MonoBehaviour
{
    public static int difficulty = 1; //Difficulty - a set integer, that increases per level (or every X levels?)
    public static int curLevel = 0;
    public static int curTime = 0; //Counts in-game hours.

    public int newIngredientInterval = 5; //Every how many days you unlock new ingredients
    public float patientDeathThresholdPercentage = 0.25f; //What percentage of patients have to die, for it to be game over

    private int prevTime; //Previous cur time, after update

    public int levelStartTime = 8;
    public int levelEndTime = 18;

    public int inGameHourToRealSeconds = 60; //How many seconds IRL is one ingame hour
    public int levelsPerDifficultyIncrease = 1; //How many levels to pass for difficulty to increase

    public int timeScoreThreshold = 2; //After how many ingame hours start deducting points

    private float internalTimer = 0;
    private float internalDayStart;

    public static bool timePause = false;

    public GameObject dayEndScreen;

    private Text clockText;

    //Define level performance indicators

    public static int ingredientsUsed;
    public static int minPossibleIngredients;
    public static float totalIngredientCount;

    public static float patientDeaths = 0f;
    public static int totalPatientCount = 0;
    public static int curedPatients = 0;

    public static int patientDeathThreshold = 0;

    public static List<List<int>> ingredientProgression = new List<List<int>>(); //List for the progression of medicine deliveries

    public static List<Dictionary<string, int>> levelScores = new List<Dictionary<string, int>>();

    #region Event Definitions

    public delegate void newDay();
    public static event newDay OnNewDay; //Define OnNewDay event, which is invoked, when day has started

    public delegate void endDay(bool levelWin, string gameOverType = "", Dictionary<string, int> levelScore = null);
    public static event endDay OnEndDay; //Define OnEndDay event, which is invoked, when day has ended

    public delegate void restartDay();
    public static event restartDay OnRestartDay; //Define OnRestartDay event, which is invoked, when a day is replayed

    #endregion

    public void startNewDay() {
        curTime = levelStartTime;
        internalDayStart = internalTimer; //Set day start reference time

        curLevel++; //Increase level

        if (curLevel % levelsPerDifficultyIncrease == 0) {
            difficulty++; //Increase difficulty, if set amount of levels have passed
        }

        //Reset level performance indicators

        ingredientsUsed = 0;
        minPossibleIngredients = 0;
        totalIngredientCount = 0;

        patientDeaths = 0;
        totalPatientCount = 0;
        curedPatients = 0;

        patientDeathThreshold = 0;

        //Update clock

        if(clockText==null) clockText = GameObject.Find("ClockText").GetComponent<Text>();

        clockText.text = $"{curTime}:00\nDay {curLevel}";

        timePause = false;

        OnNewDay?.Invoke();
    }

    public Dictionary<string, int> calculateLevelScore() {
        //Calculate ingredient efficiency score

        int scoreEfficiency = Mathf.Clamp(Mathf.RoundToInt(1000 - ((ingredientsUsed - minPossibleIngredients) / totalIngredientCount) * 1000), 0, 1000);

        //Calculate time score

        float workdayLength = levelEndTime - levelStartTime;
        float scoreTimeFloat = Mathf.Clamp(1000 * (1f - ((curTime - levelStartTime) / workdayLength - timeScoreThreshold / workdayLength)), 0, 1000);
        int scoreTime = Mathf.RoundToInt(scoreTimeFloat);

        //Calculate patient mortality score

        int scoreMortality = Mathf.Clamp(Mathf.RoundToInt(1000f * (1f - (patientDeaths - patientDeathThreshold)/totalPatientCount)), 0, 1000);

        //Calculate overall score

        int scoreOverall = (scoreEfficiency + scoreTime + scoreMortality) / 3;

        Dictionary<string, int> output = new Dictionary<string, int>() {
            { "efficiency", scoreEfficiency},
            { "time", scoreTime },
            { "mortality", scoreMortality },
            { "overall", scoreOverall }
        };

        return output;
    }

    public void endCurDay() {
        if (dayEndScreen == null) {
            GameObject[] gameObjects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
            dayEndScreen = gameObjects.SingleOrDefault(obj => obj.name == "DayEndOverlay");
        }

        dayEndScreen.SetActive(true);

        timePause = true;
        Debug.Log("End of day " + curLevel);

        if (curTime >= levelEndTime)
        { //Ran out of time
            OnEndDay?.Invoke(levelWin: false, gameOverType: "time");
        }
        else if (patientDeaths > patientDeathThreshold)
        { //Too many patients died
            OnEndDay?.Invoke(levelWin: false, gameOverType: "deaths");
        }
        else {
            //All good, you win

            Dictionary<string, int> curLevelScore = calculateLevelScore();
            levelScores.Add(curLevelScore); //Add the current level score to all level scores

            OnEndDay?.Invoke(levelWin: true, levelScore: curLevelScore);
        }
    }

    public void rewindDay() {
        if (curLevel % levelsPerDifficultyIncrease == 0) {
            difficulty--;
        }

        curLevel--;

        OnRestartDay?.Invoke();
    }

    public static void checkForIngredientUnlocks() {
        int interval = GameObject.Find("LevelManager").GetComponent<LevelManager>().newIngredientInterval;

        if ((curLevel - 1) % interval == 0)
        {
            int newIngredientIndex = (curLevel - 1) / interval;

            if (newIngredientIndex < ingredientProgression.Count)
            {
                List<int> newIngredientIDs = ingredientProgression[newIngredientIndex];

                foreach (int ingredientID in newIngredientIDs)
                {
                    inventory.lockItemID(ingredientID, locked: false); //Unlock the items
                }

                foreach (int ingredientID in newIngredientIDs)
                {
                    //Add the new possible properties

                    List<Recipe> possibleRecipes = FindRecipesWithItemID(ingredientID);

                    foreach (Recipe recipe in possibleRecipes)
                    {
                        PropertyList resultProperties = recipe.itemResult.properties;

                        foreach (Property checkProperty in resultProperties)
                        {
                            if (!Inventory.availablePropertyIDs.Contains(checkProperty.id))
                            {
                                Inventory.availablePropertyIDs.Add(checkProperty.id);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void giveMedicineShipment() {
        //Calculate minimum amount of items required

        List<Dictionary<string, int>> requiredItems = new List<Dictionary<string, int>>();

        //Get what recipes craft medicine with the required properties
        foreach (Dictionary<string, int> symptom in ClientManager.currentPatientSymptoms) {
            List<Recipe> currentRecipes = SearchRecipesForPropertyID(symptom["id"]);
            int repeatAmount = symptom["count"];
            bool hasOddRepeat = false;

            //currentRecipes = currentRecipes.Distinct().ToList(); //Remove any duplicates

            if (currentRecipes.Count > 1) {
                if (currentRecipes.Count % 2 == 0)
                { //Check if number of possible recipes is even
                    repeatAmount = Mathf.CeilToInt(symptom["count"] * 1f / currentRecipes.Count); //Divide the necessary amount between recipes
                }
                else {
                    repeatAmount = Mathf.CeilToInt(symptom["count"] - 1f / currentRecipes.Count); //Later add 1 to one of the recipes
                    hasOddRepeat = true;
                }
            }

            foreach (Recipe recipe in currentRecipes) {
                foreach (Item ingredient in recipe.ingredientList) {
                    int groupIndex = requiredItems.FindIndex(group => group["id"]==ingredient.id); //Check if the item already exists in the list
                    int ingredientCount = hasOddRepeat ? repeatAmount + 1 : repeatAmount; //If the recipe has an odd amount of repeats, add 1 to the first recipe

                    //minPossibleIngredients += ingredientCount; //Add the ingredient count before multiplying as the minimum possible

                    //Multiply the ingredients by the percentage of total score achieved the previous level. If it's the first level, multiply ingredients by 2

                    ingredientCount *= curLevel == 1 ? 2 : Mathf.RoundToInt(1f + levelScores[curLevel - 2]["overall"]/1000f);

                    totalIngredientCount += ingredientCount; //Add the ingredient count after taking into account past performance (Total count)

                    if (groupIndex < 0)
                    {
                        //If the item doesnt already exist in the list

                        Dictionary<string, int> itemGroup = new Dictionary<string, int>{
                            { "id", ingredient.id},
                            { "count", ingredientCount}
                        };

                        requiredItems.Add(itemGroup);
                    }
                    else {
                        requiredItems[groupIndex]["count"] += ingredientCount;
                    }
                    
                }

                if (hasOddRepeat) hasOddRepeat = false;
            }
        }

        //Calculate the most efficient crafting route

        foreach (ClientManager.Client patient in ClientManager.clients) {
            List<Recipe> potentialRecipes = new List<Recipe>();
            Item dummyItem = new Item(itemID: 1337);

            foreach (Property symptom in patient.symptoms) {
                List<Recipe> currentRecipes = SearchRecipesForPropertyID(symptom.id);

                //currentRecipes.RemoveAll(recipe => patient.itemHasHarmfulProperty(recipe.itemResult)); //Remove all the recipes, that craft items with patient harmful properties

                foreach (Recipe checkRecipe in currentRecipes.ToList()) {
                    foreach (Property checkProperty in checkRecipe.itemResult.properties) {
                        if (patient.harmfulProperties.inList(checkProperty)>=0) {
                            currentRecipes.Remove(checkRecipe);
                            break;
                        }
                    }
                }

                potentialRecipes.AddRange(currentRecipes);
            }

            potentialRecipes = potentialRecipes.Distinct().ToList();

            foreach (Recipe recipe in potentialRecipes) {
                foreach (Property checkProperty in patient.symptoms) {
                    if (recipe.itemResult.properties.inList(checkProperty)>=0) {
                        recipe.requiredProperties.addProperty(checkProperty);
                    }
                }
            }

            //Iterate until a compatible item is crafted
            do
            {
                //Sort recipes by the amount of required properties the result item has
                List<Recipe> sortedRecipes = potentialRecipes.OrderBy(recipe => patient.itemCompatiblePropertyCount(recipe.itemResult)).ToList();

                /*Debug.Log("Recipe readout: ");
                foreach (Recipe recipe in sortedRecipes) {
                    Debug.Log(recipe);
                }*/

                //Combine dummy item properties with the current top item

                minPossibleIngredients += 2; //Add 2 base ingredients per recipe

                if (sortedRecipes.Count == 0) {
                    Debug.LogWarning("the very nice crafting path prediction error");
                    Debug.LogWarning("Client: " + patient);
                    break;
                } //fuck this i give up

                PropertyList combinedProperties = new PropertyList(dummyItem.properties, sortedRecipes[0].itemResult.properties);

                PropertyList addedRequiredProperties = sortedRecipes[0].requiredProperties;

                dummyItem.setProperties(combinedProperties);

                potentialRecipes.Remove(sortedRecipes[0]);

                //From the potentialRecipes list: 
                //If the remaining recipes have only 1 required property, which was just added, then remove the recipe from the list
                //If the recipe has more than 1 property, then just remove the added property from the recipes required properties list, if contains the added property

                foreach (Recipe checkRecipe in potentialRecipes.ToList()) {
                    foreach (Property addedProperty in addedRequiredProperties) {
                        if (checkRecipe.requiredProperties.inList(addedProperty)>=0) {
                            if (checkRecipe.requiredProperties.count == 1) {
                                potentialRecipes.Remove(checkRecipe);
                                continue;
                            }

                            checkRecipe.requiredProperties.removePropertyByID(addedProperty.id);
                        }
                    }
                }
            }
            while (!patient.medicineIsCompatible(dummyItem));
        }

        //Debug.Log("Min possible ingredients: " + minPossibleIngredients);

        /*Debug.Log("Medicine shipment: ");

        string output = "{";

        foreach (Dictionary<string, int> itemGroup in requiredItems)
        {
            output += $"[ID: {itemGroup["id"]}, Count: {itemGroup["count"]}],\n";
        }

        Debug.Log(output + "}");*/

        foreach (Dictionary<string, int> itemGroup in requiredItems) { //Iterate through each item, set the required amount in inventory
            inventory.setAmountItemID(itemGroup["id"], itemGroup["count"]);
        }

        //Iterate through every unlocked ingredient, if it's not required in the current level, then set the amount to 0 and lock it
        foreach (int itemID in Inventory.unlockedBaseIngredients.ToList()) {
            Item checkItem = inventory.getItemByID(itemID);
            if (checkItem.count == 0)
            {
                inventory.setAmountItemID(itemID, 0);
                inventory.lockItemID(itemID, locked: true);
            }
            else if (checkItem.locked) { //If the item is locked, but it now has a count, then unlock it
                inventory.lockItemID(itemID, locked: false);
            }
        }
    }

    public static void generateIngredientProgression(int ingredientAmount = 2) {
        //Water ID - 8, other ingredients are 0 - 7.

        //Inventory inv = GameObject.Find("InventoryManager").GetComponent<InventoryManager>().inv;
        List<List<int>> itemProgression = new List<List<int>>();
        List<int> baseIngredientIDs = new List<int>();

        for (int slot = 0; slot < 8; slot++) {
            baseIngredientIDs.Add(inventory.getItemBySlot(slot).id);
        }

        for (int group = 0; group < inventory.Count / ingredientAmount; group++) {
            List<int> ingredientGroup = new List<int>();

            for (int item = 0; item < ingredientAmount; item++) {
                int randomIngredientIndex = Random.Range(0, baseIngredientIDs.Count); //Get a random index to the baseIngedientIDs list

                ingredientGroup.Add(baseIngredientIDs[randomIngredientIndex]); //Add the ingredient ID to the group

                baseIngredientIDs.RemoveAt(randomIngredientIndex); //Remove the ingredient ID, so it doesnt get picked again
            }

            itemProgression.Add(ingredientGroup); //Add the randomized group to the itemProgression list
        }

        ingredientProgression = itemProgression;
    }

    private void Start()
    {
        //startNewDay();

        clockText = GameObject.Find("ClockText").GetComponent<Text>();
    }

    private void Update()
    {
        if (!timePause) {
            internalTimer += Time.deltaTime;
            prevTime = curTime; //Set past reference
            curTime = (int)Mathf.Floor((internalTimer - internalDayStart) / inGameHourToRealSeconds) + levelStartTime; //Calculate seconds from day start, convert to ingame hours

            if (curTime != prevTime) { //Check if the game time has changed
               // Debug.Log("Current time: " + curTime);

                clockText.text = $"{curTime}:00\nDay {curLevel}";
            }

            if (curTime >= levelEndTime)
            {
                //End of day
                endCurDay();
            }
        }
    }
}
 