﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public string currentMenu = "";

    public static GameObject crafterMenu;
    public static GameObject clientMenu;
    public static GameObject journalMenu;

    public static int curClientSelected = 0;

    public delegate void MenuOpen();
    public static event MenuOpen OnMenuOpen;

    public delegate void MenuClose();
    public static event MenuClose OnMenuClose;

    public delegate void SwitchClient();
    public static event SwitchClient OnClientSwitch;

    void openMenu() {
        gameObject.SetActive(true);
        OnMenuOpen?.Invoke();
    }

    public void closeMenu() {
        gameObject.SetActive(false);

        if (currentMenu == "crafting")
        { //If crafting menu open, check if any item is in the slots
            Transform container = crafterMenu.transform;

            Transform itemSlotA = container.Find("CrafterSlotA");
            Transform itemSlotB = container.Find("CrafterSlotB");
            Transform itemSlotResult = container.Find("CrafterSlotResult");

            if (itemSlotA.childCount > 0) {
                InventoryRender.renderItem(itemSlotA.GetChild(0).GetComponent<InventoryItemData>().itemData.id);

                Destroy(itemSlotA.GetChild(0).gameObject);
            }

            if (itemSlotB.childCount > 0)
            {
                InventoryRender.renderItem(itemSlotB.GetChild(0).GetComponent<InventoryItemData>().itemData.id);

                Destroy(itemSlotB.GetChild(0).gameObject);
            }

            if (itemSlotResult.childCount > 0)
            {
                InventoryRender.renderItem(itemSlotResult.GetChild(0).GetComponent<InventoryItemData>().itemData.id);

                Destroy(itemSlotResult.GetChild(0).gameObject);
            }
        }
        else if (currentMenu == "client") {
            //Check if any medicine in slots
            Transform medSlot = clientMenu.transform.GetChild(0).Find("MedicineSlot");
            if (medSlot.childCount > 0) {
                InventoryRender.renderItem(medSlot.GetChild(0).GetComponent<InventoryItemData>().itemData.id);

                Destroy(medSlot.GetChild(0).gameObject);
            }
        }

        currentMenu = "";

        OnMenuClose?.Invoke();
    }

    public static void checkPatientSwitchBtn() {
        Button clientNextBtn = GameObject.Find("NextPatientBtn").GetComponent<Button>();
        Button clientPrevBtn = GameObject.Find("PrevPatientBtn").GetComponent<Button>();

        if (curClientSelected < ClientManager.clients.Count - 1)
        { //Unlock patient switch next button, if theere is a client after the current one
            clientNextBtn.interactable = true;
        }
        else
        {
            clientNextBtn.interactable = false;
            if (curClientSelected >= ClientManager.clients.Count) curClientSelected = ClientManager.clients.Count - 1;
        }

        if (curClientSelected > 0)
        { //Unlock prev button, if it's not the first client
            clientPrevBtn.interactable = true;
        }
        else
        {
            clientPrevBtn.interactable = false;
        }
    }

    public void openClientMenu() {
        openMenu();

        crafterMenu.SetActive(false);
        journalMenu.SetActive(false);
        clientMenu.SetActive(true);

        currentMenu = "client";

        if (curClientSelected >= ClientManager.clients.Count || curClientSelected < 0) curClientSelected = 0;

        ClientRenderer.renderClient(ClientManager.clients.getClient(curClientSelected));

        checkPatientSwitchBtn();
    }

    public void openCraftingMenu() {
        openMenu();

        clientMenu.SetActive(false);
        journalMenu.SetActive(false);
        crafterMenu.SetActive(true);

        currentMenu = "crafting";
    }

    public void openJournalMenu() {
        openMenu();

        clientMenu.SetActive(false);
        crafterMenu.SetActive(false);
        journalMenu.SetActive(true);

        GameObject.Find("JournalContainer").GetComponent<JournalRenderer>().renderAllDiscoveredRecipes();

        currentMenu = "journal";
    }

    public void clientMenuNextPatient() {
        ClientRenderer.renderClient(ClientManager.clients.getClient(++curClientSelected));

        OnClientSwitch?.Invoke();

        checkPatientSwitchBtn();
    }

    public void clientMenuPrevPatient() {
        ClientRenderer.renderClient(ClientManager.clients.getClient(--curClientSelected));

        OnClientSwitch?.Invoke();

        checkPatientSwitchBtn();
    }

    private void OnEnable()
    {
        if(crafterMenu==null) crafterMenu = GameObject.Find("Crafter");
        if (clientMenu == null) {
            clientMenu = GameObject.Find("ClientMenu");
        }
        if (journalMenu == null) journalMenu = GameObject.Find("JournalMenu");
    }
}
