﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : MonoBehaviour
{
    public SaveSystem.SaveData saveData;
    public bool loadGame = false;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
}
