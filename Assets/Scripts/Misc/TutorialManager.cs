﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    public GameObject tooltipInfo;
    public GameObject tooltipConfirm;

    public bool tutorialConfirm = false;

    public static bool showTutorialTooltip = true;

    public string[] slideText;
    public Sprite[] slideImages;

    private int currentSlide = 0;

    public delegate void SlideChange();
    public static event SlideChange OnSlideChange;

    public delegate void MenuClose();
    public static event MenuClose OnMenuClose;

    public delegate void MenuClick();
    public static event MenuClick OnMenuClick;

    public delegate void MenuOpen();
    public static event MenuOpen OnMenuOpen;

    private void updateSlideObjects() {
        if (tooltipInfo == null || tooltipConfirm == null) {
            GameObject[] gameObjects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];

            if (tooltipInfo == null)
            {
                tooltipInfo = gameObjects.SingleOrDefault(obj => obj.name == "TutorialInfo");
            }

            if (tooltipConfirm == null)
            {
                tooltipConfirm = gameObjects.SingleOrDefault(obj => obj.name == "TutorialConfirm");
            }
        }
    }

    private void updateSlide() {
        updateSlideObjects();

        tooltipInfo.transform.Find("Text").GetComponent<Text>().text = slideText[currentSlide];
        tooltipInfo.transform.Find("TutorialImage").GetComponent<Image>().sprite = slideImages[currentSlide];
    }

    private void updateControl() {
        Button prevBtn = tooltipInfo.transform.Find("ControlBtns/PrevBtn").GetComponent<Button>();
        Button nextBtn = tooltipInfo.transform.Find("ControlBtns/NextBtn").GetComponent<Button>();

        if (currentSlide == 0)
        { //Disable previous button
            prevBtn.interactable = false;
        }
        else {
            prevBtn.interactable = true;
        }

        if (currentSlide < slideText.Length - 1)
        { //Enable next button, if it's not the last slide
            nextBtn.interactable = true;
        }
        else
        { //Disable next button, if it's the last slide
            nextBtn.interactable = false;
        }

        tooltipInfo.transform.Find("ControlBtns/TutorialPageCounter").GetComponent<Text>().text = $"{currentSlide + 1}/{slideText.Length}";
    }

    public void nextSlide() {
        currentSlide++;

        OnSlideChange?.Invoke();

        updateControl();
        updateSlide();
    }

    public void prevSlide() {
        currentSlide--;

        OnSlideChange?.Invoke();

        updateControl();
        updateSlide();
    }

    public void openTutorial() {
        if (!tooltipInfo.transform.parent.gameObject.activeSelf) tooltipInfo.transform.parent.gameObject.SetActive(true);

        updateControl();
        updateSlide();

        tooltipConfirm.SetActive(false);
        tooltipInfo.SetActive(true);

        currentSlide = 0;

        OnMenuOpen?.Invoke();
    }

    public void closeTutorialConfirm() {
        tooltipConfirm.transform.parent.gameObject.SetActive(false);

        LevelManager.timePause = false;

        OnMenuClick?.Invoke();
    }

    public void neverShowTutorial() {
        closeTutorialConfirm();
        showTutorialTooltip = false;
        SaveSystem.SaveGameOptions();

        OnMenuClick?.Invoke();
    }

    public void closeTutorialInfo() {
        tooltipInfo.transform.parent.gameObject.SetActive(false);
        currentSlide = 0;

        LevelManager.timePause = false;

        OnMenuClose?.Invoke();
    }

    public void closeTooltipConfirm() {
        tooltipInfo.transform.parent.gameObject.SetActive(false);
    }

    public void openTooltipConfirm() {
        tooltipInfo.transform.parent.gameObject.SetActive(true);
    }

    private void Start()
    {
        LevelManager.timePause = true;

        if (showTutorialTooltip) {
            openTooltipConfirm();
        }
    }
}
