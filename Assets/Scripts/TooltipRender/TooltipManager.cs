﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class TooltipManager : MonoBehaviour
{
    public GameObject tooltipObj;
    public Vector2 spacing = new Vector2(5,5);

    GameObject tooltipTitle;
    GameObject tooltipText;

    RectTransform objRect;
    RectTransform tooltipRect;

    TooltipReceiver tooltipData;

    public float originalXRes = 1148;
    private float XScaleFactor;

    void updateTooltipPos(GameObject targetObj) {
        if (objRect == null || tooltipRect == null) {
            objRect = targetObj.GetComponent<RectTransform>();
            tooltipRect = tooltipObj.GetComponent<RectTransform>();
        }

        Vector2 tooltipOffset = objRect.sizeDelta / 2 + tooltipRect.sizeDelta / 2 + spacing;
        tooltipOffset /= XScaleFactor;

        tooltipObj.transform.position = targetObj.transform.position + new Vector3(tooltipOffset.x, -tooltipOffset.y);

        float rightEdgePos = tooltipRect.anchoredPosition.x + tooltipRect.sizeDelta.x / 2;
        float bottomEdgePos = tooltipRect.anchoredPosition.y - tooltipRect.sizeDelta.y / 2;

        Vector2 canvasSize = GameObject.FindGameObjectWithTag("Canvas").GetComponent<RectTransform>().sizeDelta;

        if (rightEdgePos > canvasSize.x / 2)
        {
            //Tooltip right edge outside of screen
            tooltipObj.transform.position = tooltipObj.transform.position - new Vector3(2 * tooltipOffset.x, 0);
        }

        if (bottomEdgePos < -canvasSize.y / 2)
        {
            //Tooltip bottom edge outside of screen
            tooltipObj.transform.position = tooltipObj.transform.position + new Vector3(0, 2 * tooltipOffset.y);
        }
    }

    void openTooltip(GameObject targetObj) {
        tooltipData = targetObj.GetComponent<TooltipReceiver>();

        if (tooltipObj == null) {
            GameObject[] gameObjects = Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
            tooltipObj = gameObjects.SingleOrDefault(obj => obj.tag == "TooltipObj");

            tooltipTitle = tooltipObj.transform.Find("TooltipTitle").gameObject;
            tooltipText = tooltipObj.transform.Find("TooltipText").gameObject;
        }

        tooltipObj.SetActive(true); //Enable tooltipObj

        if (tooltipData.hasTitle)
        {
            tooltipTitle.SetActive(true);
            tooltipTitle.GetComponent<Text>().text = tooltipData.tooltipTitle; //Update tooltip title, if it has one
        }
        else {
            tooltipTitle.SetActive(false); //Disable title
        }

        if (tooltipData.hasContent)
        {
            tooltipText.SetActive(true);
            tooltipText.GetComponent<Text>().text = tooltipData.tooltipContent; //Update tooltip content
        }
        else {
            tooltipText.SetActive(false);
        }

        Canvas.ForceUpdateCanvases(); //Force rescale the tooltip to fit the text

        updateTooltipPos(targetObj);
    }

    void closeTooltip() {
        tooltipObj.SetActive(false);

        //Clear everything
        objRect = null;
        tooltipRect = null;
        tooltipData = null;
    }

    void Start()
    {
        //DontDestroyOnLoad(tooltipObj);

        //Subscribe to events
        OnTooltipOpen += openTooltip;
        OnTooltipClose += closeTooltip;
        OnTooltipMove += updateTooltipPos;

        tooltipObj.SetActive(false); //Disable tooltipObj on startup

        tooltipTitle = tooltipObj.transform.Find("TooltipTitle").gameObject;
        tooltipText = tooltipObj.transform.Find("TooltipText").gameObject;

        XScaleFactor = originalXRes/Screen.width;
    }

    #region Delegate definition

    public delegate void TooltipOpen(GameObject targetObj);
    public static TooltipOpen OnTooltipOpen;

    public delegate void TooltipClose();
    public static TooltipClose OnTooltipClose;

    public delegate void TooltipMove(GameObject targetObj);
    public static TooltipMove OnTooltipMove;

    #endregion
}
