﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipReceiver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    bool tooltipOpenCountdown = false;
    bool tooltipOpen = false;
    float internalTime = 0;

    public float tooltipOpenDelay = 1.0f; //Tooltip open delay length in seconds
    public bool hasTitle = true;
    public string tooltipTitle = "";
    public bool hasContent = true;
    public string tooltipContent = "";

    public bool moveable = false;

    private Vector2 prevFramePos;
    private RectTransform rect;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Open tooltip
        tooltipOpenCountdown = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Close tooltip
        if (tooltipOpenCountdown)
        {
            tooltipOpenCountdown = false;
        }
        else if (tooltipOpen) {
            closeTooltip();
        }
    }

    void openTooltip() {
        TooltipManager.OnTooltipOpen?.Invoke(gameObject); //Invoke OnTooltipOpen delegate with tooltipContent as paramater

        tooltipOpen = true;
        tooltipOpenCountdown = false;
    }

    void closeTooltip() {
        TooltipManager.OnTooltipClose?.Invoke();
        tooltipOpen = false;
    }

    void Start() {
        rect = GetComponent<RectTransform>();
    }

    void Update() {
        if (tooltipOpenCountdown)
        {
            //Timer till tooltip open

            internalTime += Time.deltaTime;

            if (internalTime >= tooltipOpenDelay)
            {
                //Open tooltip

                openTooltip();
            }
        }
        else if (internalTime != 0) {
            internalTime = 0;
        }

        if (tooltipOpen && moveable) { //If the tooltip is open and is set as moveable, check if it has moved
            if (prevFramePos != null && prevFramePos != rect.anchoredPosition) {
                TooltipManager.OnTooltipMove?.Invoke(gameObject);
            }

            prevFramePos = rect.anchoredPosition;
        }
    }
}
